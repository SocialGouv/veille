FROM node:12-alpine

RUN apk add git

WORKDIR /app

COPY . .

RUN yarn

RUN mkdir -p clones/socialgouv

RUN git clone https://github.com/SocialGouv/legi-data.git clones/socialgouv/legi-data
RUN git clone https://github.com/SocialGouv/kali-data.git clones/socialgouv/kali-data

ENTRYPOINT ["yarn", "start"]