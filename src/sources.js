const { compare: legiCompare } = require("./dila/legi/compare");
const { compare: kaliCompare } = require("./dila/kali/compare");

const sources = {
  "socialgouv/legi-data": {
    label: "LEGI",
    require: "@socialgouv/legi-data",
    git: `https://github.com/socialgouv/legi-data`,
    cloneDir: "./clones/socialgouv/legi-data",
    compare: legiCompare
  },
  "socialgouv/kali-data": {
    label: "KALI",
    require: "@socialgouv/kali-data",
    git: `https://github.com/socialgouv/kali-data`,
    cloneDir: "./clones/socialgouv/kali-data",
    compare: kaliCompare
  }
};

module.exports = { sources };
