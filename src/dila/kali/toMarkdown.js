const diff = require("diff");

// make legifrance url
const makeUrl = (code, id) =>
  `https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=${id}&cidTexte=${code}`;

// markdown friendly diff output
const markdownDiff = (before, after) =>
  diff
    .diffWords(before, after)
    .map(diff => {
      if (diff.removed) {
        return `<del class="blob-code-delete">${diff.value}</del>`;
      } else if (diff.added) {
        return `<ins class="blob-code-addition">${diff.value}</ins>`;
      }
      return diff.value;
    })
    .join("");

// markdown diffs report
const toMarkdown = (codeId, { added, missing, modified }) => {
  let markdown = `<style>ins{background:#d0ff9a;} del{background:#ffbaba;}</style>\n\n`;

  markdown += `## Nouveaux articles (${added.length})\n\n`;
  markdown += "cid|num|etat\n";
  markdown += "---|---|----\n";
  added.forEach(art => {
    markdown += `${art.data.cid} | [${art.data.num}](${makeUrl(
      codeId,
      art.data.cid
    )}) | ${art.data.etat}\n`;
  });
  markdown += "\n\n";

  markdown += `## Articles supprimés (${missing.length})\n\n`;
  markdown += "cid|num|etat\n";
  markdown += "---|---|----\n";
  missing.forEach(art => {
    markdown += `${art.data.cid} | [${art.data.num}](${makeUrl(
      codeId,
      art.data.cid
    )}) | ${art.data.etat}\n`;
  });
  markdown += "\n\n";

  markdown += `## Articles modifiés (${modified.length})\n\n`;
  modified.forEach(art => {
    markdown += `### ${art.data.cid} - [${art.data.num}](${makeUrl(
      codeId,
      art.data.cid
    )})\n\n`;
    if (art.previous.data.content.length > 10000) {
      markdown += "(diff trop long)";
    } else {
      markdown += markdownDiff(art.previous.data.content, art.data.content);
    }
    markdown += "\n\n";
  });
  markdown += "\n\n";

  return markdown;
};

module.exports = { toMarkdown };
