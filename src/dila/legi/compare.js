const { compareArticles } = require("../compareArticles");

// return diffed nodes
const compare = (tree1, tree2) =>
  compareArticles(
    tree1,
    tree2,
    (art1, art2) =>
      art1.data.texte !== art2.data.texte || art1.data.etat !== art2.data.etat
  );

module.exports = { compare };
