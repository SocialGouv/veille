const Router = require("koa-router");

const { sources } = require("../sources");
const { sync } = require("../git/sync");
const { getFileHistory } = require("../git/getFileHistory");
const { getRepoHistory } = require("../git/getRepoHistory");

const router = new Router();

router.get("/:owner/:repo/history/:path", async ctx => {
  const { owner, repo, path } = ctx.params;
  const sourceData = sources[`${owner}/${repo}`];
  // allow-list only
  if (sourceData) {
    console.log("sync...");
    await sync(sourceData);
    console.log("getFileHistory...");
    ctx.body = await getFileHistory({
      cloneDir: sourceData.cloneDir,
      path
    });
  }
});

router.get("/:owner/:repo/history", async ctx => {
  const { owner, repo } = ctx.params;
  const sourceData = sources[`${owner}/${repo}`];
  // allow-list only
  if (sourceData) {
    console.log("sync...");
    await sync(sourceData);
    console.log("getRepoHistory...");
    ctx.body = await getRepoHistory({ cloneDir: sourceData.cloneDir });
  }
});

module.exports = router;
