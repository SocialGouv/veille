const Router = require("koa-router");

const { sources } = require("../sources");
const { sync } = require("../git/sync");
const { getFileHistory } = require("../git/getFileHistory");
const { compareJson } = require("../compareJson");

const router = new Router();

const getPreviousSha = async (sourceData, path, sha) => {
  const history = Array.from(
    await getFileHistory({
      cloneDir: sourceData.cloneDir,
      path
    })
  );
  const idx = history.findIndex(log => log.hash === sha);
  const previous =
    idx > -1 && idx < history.length - 1 && history[idx + 1].hash;

  return previous;
};

router.get("/:owner/:repo/diff/:sha/:path", async ctx => {
  const { owner, repo, sha, path } = ctx.params;
  const sourceData = sources[`${owner}/${repo}`];
  // allow-list only
  if (sourceData) {
    const previousSha = await getPreviousSha(sourceData, path, sha);
    await sync(sourceData);
    ctx.body = await compareJson({
      compare: sourceData.compare,
      cloneDir: sourceData.cloneDir,
      path,
      sha1: previousSha,
      sha2: sha
    });
  }
});

module.exports = router;
