const Router = require("koa-router");

const { sources } = require("../sources");
const { getJsonFile } = require("../git/getJsonFile");
const { sync } = require("../git/sync");
const { compareJson } = require("../compareJson");

const router = new Router();

/*
http://127.0.0.1:1337/socialgouv/legi-data/compare/ec2f83d4f22cae2566010d9d7941bfb715f1a5f8/72c09001f35bbaac7c59446e982317f028cb3d81/data%2Findex.json
http://127.0.0.1:1337/socialgouv/kali-data/compare/e5d4a43037c2cd3cc719b7cd8aa37362e000d899/7f0b183c24f2750ac3c3494bd7d81d9f862cae7d/data%2FKALICONT000005635618.json
*/

router.get("/:owner/:repo/compare/:sha1/:sha2/:path", async ctx => {
  const { owner, repo, sha1, sha2, path } = ctx.params;
  const sourceData = sources[`${owner}/${repo}`];
  if (sourceData) {
    console.log("sync...");
    await sync(sourceData);
    console.log("compare...");
    // AST comparison
    if (sourceData.compare && path.endsWith(".json")) {
      ctx.body = await compareJson({
        compare: sourceData.compare,
        cloneDir: sourceData.cloneDir,
        path,
        sha1,
        sha2
      });
    }
  }
});

module.exports = router;
