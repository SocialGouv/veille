const Router = require("koa-router");
const serialExec = require("promise-serial-exec");

const { sources } = require("../sources");
const { getLatestChanges } = require("../git/getLatestChanges");
const { sync } = require("../git/sync");
const { compareJson } = require("../compareJson");

const router = new Router();

const filterPath = path =>
  path &&
  // filter out interesting files
  path.match(/^data\/(LEGITEXT|KALICONT)\d+\.json$/);

const getFileInfos = (sourceData, path) => {
  try {
    const content = require(`${sourceData.require}/${path}`);
    return {
      title: content.data.title,
      etat: content.data.etat,
      id: content.data.id
    };
  } catch (e) {}
  return {};
};

router.get("/:owner/:repo/latest", async ctx => {
  const { owner, repo } = ctx.params;
  const sourceData = sources[`${owner}/${repo}`];
  const showDetails = ctx.query.detail !== undefined;
  if (sourceData) {
    await sync(sourceData);
    const changes = await Promise.all(
      (
        await getLatestChanges({
          cloneDir: sourceData.cloneDir,
          filterPath
        })
      ).map(async (change, i, all) => ({
        source: sourceData.label,
        ...change,
        files: await serialExec(
          change.files.map(file => async () => ({
            path: file,
            info: getFileInfos(sourceData, file)
          }))
        )
      }))
    );
    ctx.body = changes;
  }
});

module.exports = router;
