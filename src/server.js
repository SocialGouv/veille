const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const cors = require("@koa/cors");

const health = require("./routes/health");
const diff = require("./routes/diff");
const history = require("./routes/history");
const compare = require("./routes/compare");
const latest = require("./routes/latest");

const app = new Koa();
const PORT = process.env.PORT || 5000;

app.use(cors());
/**
 * use a middleware for catching errors and re-emit them
 * so we can centralize error handling / logging
 * https://github.com/koajs/koa/wiki/Error-Handling
 */
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    console.log(error);
    const { statusCode = 500, message } = error;
    ctx.throw(statusCode, message);
    ctx.app.emit("error", error);
  }
});

app.use(bodyParser());

app.use(health.routes());
app.use(diff.routes());
app.use(history.routes());
app.use(compare.routes());
app.use(latest.routes());

if (process.env.NODE_ENV !== "production") {
  console.log("-- DEV MODE ---");
}

// centralize error logging
app.on("error", ({ statusCode, message }) => {
  console.log(`${statusCode} - ${message}`);
});

// Server.
const server = app.listen(PORT, () => {
  console.log(`Server listening on http://127.0.0.1:${PORT}`);
});

module.exports = server;
